package com.company.springdemo.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class LoggingAspect
{
	private Logger myLogger = Logger.getLogger(LoggingAspect.class.getName());
	
	@Pointcut("execution(* com.company.springdemo.controller.*.*(..))")
	private void forControllerPackage()
	{
	}
	
	@Pointcut("execution(* com.company.springdemo.service.*.*(..))")
	private void forServicePackage()
	{
	}
	
	@Pointcut("execution(* com.company.springdemo.dao.*.*(..))")
	private void forDAOPackage()
	{
	}
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDAOPackage()")
	private void combinedPointcutForApp()
	{
	}
	
	@Before("combinedPointcutForApp()")
	public void before(JoinPoint joinpoint)
	{
		myLogger.info("=====> in @Before calling method: " + joinpoint.getSignature());
		Object[] o = joinpoint.getArgs();
		
		for (Object ob : o)
		{
			myLogger.info("=====> argument: " + ob);
		}
	}
	
	@AfterReturning(pointcut = "combinedPointcutForApp()",returning = "result")
	public void afterReturning(JoinPoint joinPoint, Object result)
	{
		myLogger.info("=====> in @AfterReturning from method: " + joinPoint.getSignature());
		myLogger.info("=====> result: " + result);
	}
	
}
